function Calcular() {
    //Función con código generado por los docentes para cargar los 4 números ingresados
    // en el array "valores" y en las 4 variables (v1, v2, v3 y v4)
    valores.Act_Valores();
    //A partir de acá, van a llamar a las funciones que ustedes generen para:
    //1) Mostrar un alert con la suma de todos los números ingresados.
    alert("La suma total es: " + valores.Sumatoria());
    //2) Mostrar un alert con el mayor número de los 4 ingresados.
    alert("El valor mayor es: " + valores.CalMayor());
    //3) Mostrar un alert con los dos números del medio (ni el mayor, ni el menor)
    alert("OP1) Los valores del medio son: " + valores.CalMedio()[0] + " y " + valores.CalMedio()[1] );
    //4) Mostrar por consola los 4 números, en orden inverso (primero el 4, último el 1) utilizando un solo console.log
    //alert("Invertir: " + invertir());
    //5) Realizar una calculadora, utilizando el 1er y 2do número como operandos, y el 3ro como indicador de suma (0), resta (1), multiplicación (2) o división (3)
    //alert("Calculadora es: " + Calculadora());
}
/*

function invertir(){
    let aux = new Array(valores.length);
    for (let i = 0; i < valores.length; i++ ){
        aux[valores.length-1-i]=valores[i];
    }
    return aux;
}
function Calculadora(){
    switch(valores[2]){
        case 0:
            return (valores[0]+valores[1]);
        break;
        case 1:
            return (valores[0]-valores[1]);
        break;
        case 2:
            return (valores[0]*valores[1]);
        break;
        case 3:
            if (valores[1]==0){
                valores[1]=1;
            }
            return (valores[0]/valores[1]);
        break;
        default:
            alert("error ");    
    }
}
*/