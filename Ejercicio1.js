//Código interno que genera un vector con los valores
var valores = {
    v: new Array(4),
    v1: undefined, v2: undefined, v3: undefined, v4: undefined,
    Act_Valores: function()
    {
        let temp = document.getElementsByName("numeros");
        for(let i = 0; i < this.v.length; i++) {
            this.v[i] = parseInt(temp[i].value);
        };
        this.v1 = this.v[0];
        this.v2 = this.v[1];
        this.v3 = this.v[2];
        this.v4 = this.v[3];
        console.log("Valores cargados por el docente: ", this.v, this.v1, this.v2, this.v3, this.v4);
    },
    Sumatoria: function(){
        let suma=0;
        for (let i = 0; i < this.v.length; i++ ){
            suma+=this.v[i];
        }
        return suma;
    },
    CalMayor: function(){
        let mayor = 0;
        for (let i = 0; i < this.v.length; i++ ){
            if(mayor < this.v[i]){
                mayor = this.v[i];
            }
        }
        return mayor;
    },
    CalMenor: function(){
        let menor= this.CalMayor();
        for (let i = 0; i < this.v.length; i++ ){
            if(menor > this.v[i]){
                menor = this.v[i];
            }
        }
        return menor;
    },
    CalMedio: function(){
        let medio = new Array();
        for (let i = 0; i < this.v.length; i++ ){
            if( (this.v[i]!=this.CalMayor()) && (this.v[i]!=this.CalMenor()) && (medio[0]==undefined) ){
                medio[0]=this.v[i];
            }
            if( (this.v[i]!=this.CalMayor()) && (this.v[i]!=this.CalMenor()) && (medio[0]!=this.v[i]) ){
                medio[1]=this.v[i];
            }
        }
        return medio;
    }
}